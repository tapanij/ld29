﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

		private class ControlState
		{
				public float horizontal = 0f;
				public float vertical = 0f;
				public bool stroke = false;
				public bool kicking = false;

				public ControlState ()
				{

				}
		}


		private ControlState cstate = new ControlState ();

		// CONSTANTS
		private float maxSpeed = 2.5f;
		private float strokeForce = 2f;
		private float kickingForce = 7f;
		private float horizontalTorqueForce = 5f;
		private float verticalTorqueForce = 3f;
		private float minX = -0.9f;
		private float maxX = 0.9f;
		private float minAngX = 60f;
		private float maxAngX = 300f;
		private float maxAngVelo = 2f;
		private float kickingDelay = 0.5f;
		private float idleDelay = 0.7f;

		// ANIMATIONS
		private Animation _animation;
		public AnimationClip idleAnimation;
		public AnimationClip strokeAnimation;
		public AnimationClip kickingAnimation;

		//
		private int collectedPearls = 0;
		public GUIText guiPearl;
		private float pearlDistance = 0f;
		private bool gameEnded = false;
		private int extraPearls = 1;

		// AUDIO
		private int radarMinDistance = 1000;
		private float minRadarVolume = 0.3f;
		private float minRadarPitch = 0.3f;
		public AudioSource radar;
		public AudioSource stroke;
		public AudioSource pearl;



//		// RADAR
//		public AudioSource radarworks;
//	public AudioSource radardoesnt;

		// Use this for initialization
		void Start ()
		{
				rigidbody.maxAngularVelocity = maxAngVelo;
				_animation = GetComponentInChildren<Animation> ();

				if (!_animation)
						Debug.Log ("The character you would like to control doesn't have animations. Moving her might look weird.");
				if (!idleAnimation) {
						_animation = null;
						Debug.Log ("No idle animation found. Turning off animations.");
				}
				if (!strokeAnimation) {
						_animation = null;
						Debug.Log ("No strokeAnimation found. Turning off animations.");
				}
				if (!kickingAnimation) {
						_animation = null;
						Debug.Log ("No kickingAnimation found. Turning off animations.");
				}

		
				UpdateGUI ();
		}
	
		// Update is called once per frame
		void PlayerUpdate ()
		{
				cstate.horizontal = Input.GetAxis ("Horizontal");
				cstate.vertical = Input.GetAxis ("Vertical");

				if (!cstate.stroke) {
						cstate.stroke = Input.GetButtonDown ("Stroke");
				}

				if (Input.GetButtonDown ("Stroke")) {
						Invoke ("SetKicking", kickingDelay);
						Invoke ("SetIdle", idleDelay);
//						Debug.Log ("invoke SetKicking");
				} 
				if (IsInvoking ("SetKicking") && !Input.GetButton ("Stroke")) {
						CancelInvoke ("SetKicking");
//						Debug.Log ("cancel SetKicking");
				} else if (cstate.kicking && !Input.GetButton ("Stroke")) {
						cstate.kicking = false;
						_animation.Play (idleAnimation.name);
//						Debug.Log ("kicking false");
				}

				if (Input.GetButtonDown ("Radar")) {
						UseRadar ();
				}

				// End game
				if (gameEnded) {
						if (Vector3.Distance (transform.position, Vector3.zero) > transform.parent.GetComponent<GameController> ().areaRadius) {
								Application.LoadLevel (1);
						}
				}
		}

		void UseRadar ()
		{		
				GameObject closestPearl = FindClosestGameObject ("pearl");
				Vector3 forward = transform.TransformDirection (Vector3.forward);
				Vector3 toOther = closestPearl.transform.position - transform.position;
				toOther.Normalize ();
				float dotProduct = Vector3.Dot (forward, toOther);
				
//				Debug.Log ("closesPearl " + closestPearl.transform.GetInstanceID ());
				Debug.Log (dotProduct);

				audio.pitch = Mathf.Max (minRadarPitch, dotProduct);
				audio.volume = Mathf.Max (minRadarVolume, (radarMinDistance - pearlDistance) / radarMinDistance);




//				Debug.Log ("audio.pitch: " + audio.pitch);
//				Debug.Log ("audio.volume: " + audio.volume);
//				Debug.Log ("dotProduct: " + dotProduct);
//				Debug.Log ("pearlDistance: " + pearlDistance);
				radar.Play ();
				
		}
	
		GameObject FindClosestGameObject (string tag)
		{
				GameObject[] gos;
				gos = GameObject.FindGameObjectsWithTag (tag);
				GameObject closest = null;
				float distance = Mathf.Infinity;
				

				foreach (GameObject go in gos) {
						Vector3 diff = go.transform.position - transform.position;
						float curDistance = diff.sqrMagnitude;
						if (curDistance < distance) {
								closest = go;
								distance = curDistance;
						}
				}

				pearlDistance = distance;

				return closest;
		}

		void SetIdle ()
		{
				if (IsInvoking () || cstate.kicking) {
						return;
				}
				_animation.Play (idleAnimation.name);
		}
	
		void SetKicking ()
		{
//				Debug.Log ("SetKicking true");
				cstate.kicking = Input.GetButton ("Stroke");
		}

		void PlayerFixedUpdate ()
		{
				// STROKE
				if (cstate.stroke) {
						if (rigidbody.velocity.magnitude < maxSpeed) {
								rigidbody.AddForce (transform.forward * strokeForce, ForceMode.Impulse);
								if (_animation) {
										Debug.Log ("Stroke animation");
										_animation.Stop ();
										_animation.Play (strokeAnimation.name);
										stroke.Play ();
								} else {
										Debug.LogWarning ("_animation not set!");
								}
						} else { 
//								Debug.Log ("cap");
						}
						cstate.stroke = false;
//						Debug.Log ("Stroke");
				} else if (cstate.kicking) {
						// KICKING
						if (rigidbody.velocity.magnitude < maxSpeed) {
								rigidbody.AddForce (transform.forward * kickingForce, ForceMode.Acceleration);
								if (_animation) {
										Debug.Log ("Kicking animation");
										_animation.CrossFade (kickingAnimation.name);
								} else {
										Debug.LogWarning ("_animation not set!");
								}
						} else { 
								//								Debug.Log ("cap");
						}
				}

				// TORQUE

				rigidbody.AddTorque (transform.up * horizontalTorqueForce * cstate.horizontal, ForceMode.Acceleration);
				rigidbody.AddTorque (transform.right * verticalTorqueForce * cstate.vertical, ForceMode.Acceleration);
	
				// Cap rotation
				float dotProduct = Vector3.Dot (transform.forward, Vector3.up);
		
				if (dotProduct < -0.5f && transform.rotation.eulerAngles.x > minAngX) {
//			Debug.Log ("Cap" + transform.rotation.eulerAngles);
						transform.rotation = Quaternion.Euler (minAngX, transform.rotation.eulerAngles.y, 0);
				} else if (dotProduct > 0.5f && transform.rotation.eulerAngles.x < maxAngX) {
//						Debug.Log ("Cap2" + transform.rotation.eulerAngles);
						transform.rotation = Quaternion.Euler (maxAngX, transform.rotation.eulerAngles.y, 0);
				}

				// Freeze local Z axis rotation
				transform.rotation = Quaternion.Euler (transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
		}

		void UpdateGUI ()
		{
				guiPearl.text = collectedPearls + " / " + (transform.parent.GetComponent<GameController> ().pearlAmount - extraPearls);
		}

		void OnCollisionEnter (Collision collision)
		{
				if (collision.gameObject.name.StartsWith ("Pearl")) {
						Destroy (collision.gameObject);
						collectedPearls++;
						UpdateGUI ();
						pearl.Play ();

						if (collectedPearls >= (transform.parent.GetComponent<GameController> ().pearlAmount - extraPearls)) {
								SendMessageUpwards ("EndGame");
								gameEnded = true;
						}
				}

		}
}
