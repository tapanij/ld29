﻿using UnityEngine;
using System.Collections;

public class PlantRandomizer : Randomizer {

	private Vector3[] baseVertices;
	int seed;
	float firstRandom;
	
	// Use this for initialization
	void Start ()
	{
		seed = Random.seed = Random.Range (1,10000);
		
		firstRandom = Random.value;
		Debug.Log (firstRandom);
		
		// LENGTH & SCALE & MASS
		float lengthScale = Random.Range (0.1f, 2f);
		
		float newScale = Random.Range (0.05f, 1f);
		transform.localScale = new Vector3 (newScale, newScale, lengthScale);

		
		// MESH
		Mesh mesh = GetComponent<MeshFilter> ().mesh;
		
		baseVertices = mesh.vertices;
		
		Vector3[] vertices = new Vector3[baseVertices.Length];
		
		for (int i = 0; i < vertices.Length; i++) {
			vertices [i] = Randomize (baseVertices [i], i); 
		}
		
		mesh.vertices = vertices;
		mesh.RecalculateBounds ();
		
		// MATERIAL
		renderer.material.shader = Shader.Find ("Specular");
		renderer.material.SetColor ("_Color", new Color (Random.value, Random.value, Random.value, Random.value));
		renderer.material.SetColor ("_SpecColor", new Color (Random.value, Random.value, Random.value, Random.value));
		
		// Collider
		gameObject.AddComponent <MeshCollider> ();
//		GetComponent <MeshCollider> ().convex = true;
		GetComponent <MeshCollider> ().sharedMesh = mesh;
		
	}
	
	Vector3 Randomize (Vector3 pos, int i)
	{
		//				Random.seed = seed++;
		//				Vector3 newPos = new Vector3 (Random.Range (-1f, 1f), Random.Range (-1f, 1f), Random.Range (-1f, 1f));
		float newPerlin = Mathf.PerlinNoise (pos.x + firstRandom, pos.y + firstRandom) * 4f - 2f;
		
//		Random.seed = (int)(seed + (pos.y * 10f));
		float rnd = Random.Range (-2f,2f);
		float xOffset = (newPerlin + rnd) * Mathf.Sign (pos.x);
		float zOffset = (newPerlin + rnd) * Mathf.Sign (pos.z);
		float yOffset = (xOffset + zOffset) / 2f;
		
		Vector3 newPos = new Vector3 (xOffset + pos.x, yOffset+pos.y, zOffset + pos.z);
		//				Debug.Log ("----");
		//				Debug.Log (rnd);
		//				Debug.Log (Random.seed);
		//				Debug.Log (pos.y);
		return newPos;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
}
