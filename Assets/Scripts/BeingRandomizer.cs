﻿using UnityEngine;
using System.Collections;

public class BeingRandomizer : Randomizer {

	private Vector3[] baseVertices;
	int seed;
	float firstRandom;
	
	// Use this for initialization
	void Start ()
	{
		seed = Random.seed = Random.Range (1,10000);
		
		firstRandom = Random.value;
		Debug.Log (firstRandom);
		
		// LENGTH & SCALE & MASS
		Vector3 lengthScale = transform.localScale;
		lengthScale.y = Random.Range (3f, 10f);
		transform.localScale = lengthScale;
		
		float parentScale = Random.Range (0.01f, 0.6f);
		transform.parent.localScale = new Vector3 (parentScale, parentScale, parentScale);
		transform.parent.rigidbody.mass = parentScale * 80f;
		
		// MESH
		Mesh mesh = GetComponent<MeshFilter> ().mesh;
		
		baseVertices = mesh.vertices;
		
		Vector3[] vertices = new Vector3[baseVertices.Length];
		
		for (int i = 0; i < vertices.Length; i++) {
			vertices [i] = Randomize (baseVertices [i], i); 
		}
		
		mesh.vertices = vertices;
		mesh.RecalculateBounds ();
		
		// MATERIAL
		renderer.material.shader = Shader.Find ("Specular");
		renderer.material.SetColor ("_Color", new Color (Random.value, Random.value, Random.value, Random.value));
		renderer.material.SetColor ("_SpecColor", new Color (Random.value, Random.value, Random.value, Random.value));
		
		// Collider
		gameObject.AddComponent <MeshCollider> ();
		GetComponent <MeshCollider> ().convex = true;
		GetComponent <MeshCollider> ().sharedMesh = mesh;
		
	}
	
	Vector3 Randomize (Vector3 pos, int i)
	{
		//				Random.seed = seed++;
		//				Vector3 newPos = new Vector3 (Random.Range (-1f, 1f), Random.Range (-1f, 1f), Random.Range (-1f, 1f));
		float newPerlin = Mathf.PerlinNoise (pos.x + firstRandom, pos.y + firstRandom) * 4f - 2f;
		
		Random.seed = (int)(seed + (pos.y * 10f));
		float rnd = Random.value * 2f;
		float xOffset = (newPerlin + rnd) * Mathf.Sign (pos.x);
		float zOffset = (newPerlin + rnd) * Mathf.Sign (pos.z);
		
		Vector3 newPos = new Vector3 (xOffset + pos.x, pos.y, zOffset + pos.z);
		//				Debug.Log ("----");
		//				Debug.Log (rnd);
		//				Debug.Log (Random.seed);
		//				Debug.Log (pos.y);
		return newPos;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
}
