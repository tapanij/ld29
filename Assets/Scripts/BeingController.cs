﻿using UnityEngine;
using System.Collections;

public class BeingController : MonoBehaviour
{
		private float torqueForce = 0.15f;
		private float areaRadius = 100f;
		private float maxHeight = 40f;
		private Vector3 middlePoint = new Vector3 (0f, 5f, 0f);
		private bool followPlayer = false;
		private GameObject player;

		// Use this for initialization
		void Start ()
		{
				// TODO constraints, forcemode, force range

		}
	
		// Update is called once per frame
		void Update ()
		{
				// Don't try to escape!
				if (transform.position.y > maxHeight) {
//						Debug.Log ("turnaround 1");
						TurnAround ();
				} else if (transform.parent) {
						float distance = Vector3.Distance (transform.position, transform.parent.position);
						if (distance > areaRadius) {
//								Debug.Log ("turnaround 2");
								TurnAround ();
						}
				}

				// Don't get stuck!
				if (rigidbody.velocity.magnitude < 0.1f) {
//						Debug.Log ("stuck! " + rigidbody.velocity.magnitude);
						Vector3 relativePos = middlePoint - transform.position;
			relativePos.Normalize ();
						relativePos *= 20f;
						rigidbody.AddForce (relativePos, ForceMode.Impulse);
				}

				// Follow player
				if (followPlayer) {
						if (!player) {
								player = GameObject.FindGameObjectWithTag ("Player");
						}
						Vector3 relativePos = player.transform.position - transform.position;
						relativePos.Normalize ();
						Quaternion rotation = Quaternion.LookRotation (relativePos);
						transform.rotation = rotation;
				}
		}

		void FollowPlayer ()
		{
				followPlayer = true;
		}

		void TurnAround ()
		{
				Vector3 relativePos = middlePoint - transform.position;
				relativePos.Normalize ();
				Quaternion rotation = Quaternion.LookRotation (relativePos);
				transform.rotation = rotation;

//		transform.rotation = Quaternion.Inverse(transform.rotation);
		}

		void FixedUpdate ()
		{
				// TORQUE
				Vector3 randomDirection = new Vector3 ();
				Random.seed = (int)Time.time;
				randomDirection.x = Random.Range (-1f, 1f);
				Random.seed++;
				randomDirection.y = Random.Range (-1f, 1f);
				Random.seed++;
				randomDirection.z = Random.Range (-1f, 1f);

				randomDirection.Normalize ();
				float randomForce = Random.Range (0f, 10f);

//				Debug.Log (randomDirection);
//				Debug.Log (randomForce);

				rigidbody.AddTorque (randomDirection * randomForce * torqueForce, ForceMode.Acceleration);

				// VELOCITY
				rigidbody.AddForce (transform.forward * randomForce, ForceMode.Acceleration);
		}
}
