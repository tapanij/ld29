﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
		public GameObject[] plantPrefabs;
		public GameObject[] beingPrefabs;
		public GameObject pearlPrefab;
		private int plantAmount = 90;
		private int beingAmount = 35;
		public int pearlAmount;
		public int areaRadius = 100;
		private bool gameStarted = false;
		public Camera introCam;
		public Camera playerCam;
		public GameObject introGUI;
		public GUIText helpText;
		public GUIText endText;

		// Use this for initialization
		void Start ()
		{
				// PLANTS
				for (int i = 0; i < plantAmount; i++) {
						Vector2 newPosition = Random.insideUnitCircle * areaRadius;
						Vector3 rndRotation = new Vector3 (270f, Random.Range (0f, 360f), 0f);
						Quaternion newQuat = new Quaternion ();
						newQuat.eulerAngles = rndRotation;
						GameObject newPlant = Instantiate (plantPrefabs [Random.Range (0, plantPrefabs.Length - 1)], new Vector3 (newPosition.x, 1f, newPosition.y), newQuat) as GameObject; 
				}

				// BEINGS
				for (int i = 0; i < beingAmount; i++) {
						Vector2 newPosition = Random.insideUnitCircle * areaRadius;
						Vector3 rndRotation = new Vector3 (270f, Random.Range (0f, 360f), 0f);
						Quaternion newQuat = new Quaternion ();
						newQuat.eulerAngles = rndRotation;
						GameObject newBeing = Instantiate (beingPrefabs [Random.Range (0, beingPrefabs.Length - 1)], new Vector3 (newPosition.x, 3f, newPosition.y), newQuat) as GameObject;
						newBeing.transform.parent = transform;
//						newBeing.transform.GetComponent<BeingController> ().areaRadius = areaRadius;
				}

				// PEARLS
				int staticPearlAmount = GameObject.FindGameObjectsWithTag ("pearl").Length;
				Debug.Log ("staticPearlAmount" + staticPearlAmount);

				for (int i = 0; i < pearlAmount - staticPearlAmount; i++) {
						Vector2 newPosition = Random.insideUnitCircle * areaRadius;
						Instantiate (pearlPrefab, new Vector3 (newPosition.x, 1f, newPosition.y), Quaternion.identity); 
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (gameStarted) {
						BroadcastMessage ("PlayerUpdate");
				}

				/** Fullsreen switch **/
				if (Input.GetButtonUp ("FullscreenToggle")) {
						Debug.Log ("fullscreen");
						Screen.fullScreen = !Screen.fullScreen;
			
			
						if (Screen.fullScreen) {
								// Set your windowed resolution to whatever you want here.
								Screen.SetResolution (1152, 648, false);
						} else {
								// Switch to the desktop resolution in fullscreen mode.
								Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.height, true);
						}
				}
		}

		void FixedUpdate ()
		{
				if (gameStarted) {
						BroadcastMessage ("PlayerFixedUpdate");
				} else {
						BroadcastMessage ("RotateCamera");
				}
		}

		void StartGame ()
		{
				introCam.enabled = false;
				playerCam.enabled = true;
				Destroy (introGUI);
				helpText.enabled = true;
				Destroy (helpText, 8f);
				gameStarted = true;
		}

		void EndGame ()
		{
				endText.enabled = true;
				BroadcastMessage ("FollowPlayer");
		}
}
